FROM openjdk:21
WORKDIR /app
COPY ./build/libs/ecs-*.jar /app/app.jar
ENTRYPOINT ["java", "-jar", "app.jar"]

