package com.example.ecs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class HealthService {

    private final String appName;

    public HealthService(@Value("${spring.application.name}") String appName) {
        this.appName = appName;
    }
    public String health() {
        return "Deployment with task def in command";
    }
}
